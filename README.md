**Tugas Besar: Genshin Impact**

Nama: Banie Al'Afaf

NIM: 1187050014

Kelas: Praktikum PBO A

**Deskrispi Aplikasi**

Genshin Impact adalah permainan video aksi petualangan gratis-to-play dengan elemen RPG dan open-world. Pemain memainkan karakter bernama Traveler yang dapat dipersonalisasi dan menjelajahi dunia Teyvat, berinteraksi dengan NPC, menyelesaikan quest, mengumpulkan item, dan melawan musuh dengan sistem pertarungan real-time. Game ini juga menawarkan sistem gacha opsional untuk membuka karakter baru dan item lainnya.

**Teknologi yang Digunakan**
- Bahasa Pemrograman: Python
- Framework: -
- Database: -

**USE CASE PRIORITY**
## Use Case Prioritas dan Use Case Scenario dari Gameplay Genshin Impact

### Use Case Prioritas

| No | Nama Use Case | Prioritas |
| -- | ------------ | --------- |
| 1  | Memulai Game | Tinggi    |
| 2  | Mekanik Menyerang | Tinggi   |
| 3  | Mekanik Game Over | Tinggi |
| 4  | Mekanik Membuat Hero Berjalan | Tinggi |
| 5  | Mekanik HP Berkurang | Sedang |

### Use Case Scenario

1. Memulai Game
   - User membuka aplikasi Genshin Impact
   - User menekan tombol "Start Game"
   - Sistem memuat data awal dan tampilan menu utama
   
2. Memilih Karakter
   - User memilih menu "Characters"
   - User memilih karakter yang ingin dimainkan
   - Sistem menampilkan detil karakter
   
3. Menyelesaikan Quest Utama
   - User memilih quest utama dari menu "Quests"
   - Sistem menunjukkan arah dan jarak ke tujuan quest
   - User menjelajahi dunia game dan menyelesaikan quest utama
   
4. Memilih Peta Tempat Bermain
   - User memilih menu "Maps"
   - User memilih peta tempat bermain yang ingin dijelajahi
   - Sistem menampilkan peta dan lokasi di dalamnya
   
5. Menyelesaikan World Quest
   - User memilih world quest dari menu "Quests"
   - Sistem menunjukkan arah dan jarak ke tujuan quest
   - User menjelajahi dunia game dan menyelesaikan world quest
   
6. Berinteraksi dengan NPC
   - User menemukan NPC di dalam dunia game
   - User menekan tombol "Interact" untuk berbicara dengan NPC
   - Sistem menampilkan dialog dan pilihan interaksi
   
7. Bermain dengan Teman
   - User membuka menu "Multiplayer"
   - User bergabung dengan sesi game teman atau membuat sesi sendiri
   - Sistem menghubungkan pemain dan menampilkan tampilan game bersama
   
8. Membeli Item dengan Uang Asli
   - User memilih menu "Shop"
   - User memilih item yang ingin dibeli
   - User melakukan transaksi pembayaran menggunakan uang asli
   - Sistem mengirimkan item ke akun user
   
## Class Diagram

```
+------------------+       +-----------------+       +---------------+
| Player           |       | Character       |       | NPC           |
+------------------+       +-----------------+       +---------------+
| - name: string   |       | - name: string  |       | - name: string|
| - level: int     |       | - level: int    |       | - quest: Quest|
| - exp: int       |       | - exp: int      |       | - dialog: []  |
| - stamina: int   |       | - weapon: Weapon|       +---------------+
| - quests: Quest[]|       +-----------------+
| - inventory: []  |
+------------------+

+-------------------+       +-----------------+       +---------------+
|+-------------------+       +-----------------+       +---------------+
| Weapon            |       | Quest           |       | Shop          |
+-------------------+       +-----------------+       +---------------+
| - name: string    |       | - name: string  |       | - items: []   |
| - type: string    |       | - reward: []    |       | - price: int  |
| - rarity: string  |       | - status: string|       +---------------+
| - damage: int     |       +-----------------+
+-------------------+

```

Penjelasan class diagram:

- `Player`: class untuk merepresentasikan pemain dalam game Genshin Impact. Memiliki atribut seperti `name`, `level`, `exp`, `stamina`, `quests`, dan `inventory`.
- `Character`: class untuk merepresentasikan karakter dalam game Genshin Impact. Memiliki atribut seperti `name`, `level`, `exp`, dan `weapon`.
- `NPC`: class untuk merepresentasikan karakter non-player dalam game Genshin Impact. Memiliki atribut seperti `name`, `quest`, dan `dialog`.
- `Weapon`: class untuk merepresentasikan senjata dalam game Genshin Impact. Memiliki atribut seperti `name`, `type`, `rarity`, dan `damage`.
- `Quest`: class untuk merepresentasikan quest dalam game Genshin Impact. Memiliki atribut seperti `name`, `reward`, dan `status`.
- `Shop`: class untuk merepresentasikan toko dalam game Genshin Impact. Memiliki atribut seperti `items` dan `price`.



Berikut adalah list use case prioritas dari gameplay Genshin Impact:

| No. | Use Case Prioritas | Deskripsi |
| --- | --- | --- |
| 1 | Menjelajahi Teyvat | User dapat menjelajahi dunia Teyvat dengan kebebasan untuk menentukan rute yang ingin diambil. |
| 2 | Bertarung dengan Musuh | User dapat bertarung dengan musuh menggunakan sistem pertarungan yang bervariasi dan mendebarkan. |
| 3 | Memilih Karakter | User dapat memilih karakter yang sesuai dengan gaya bermain mereka dan membangun tim karakter yang kuat. |
| 4 | Meningkatkan Level Karakter | User dapat meningkatkan level dan skill karakter mereka untuk meningkatkan kemampuan bertarung dan mengakses konten yang lebih sulit. |
| 5 | Mendapatkan Senjata dan Peralatan Baru | User dapat mendapatkan senjata dan peralatan baru untuk memperkuat karakter mereka. |
| 6 | Memainkan Story Quests | User dapat memainkan story quest untuk mengikuti cerita utama dan mengalami pengalaman storytelling yang menarik. |
| 7 | Memainkan World Quests | User dapat memainkan world quest untuk menambah pengalaman dan mendapatkan hadiah. |
| 8 | Berinteraksi dengan NPC | User dapat berinteraksi dengan karakter non-player dalam game untuk memperoleh informasi atau quest baru. |
| 9 | Memainkan Event | User dapat memainkan event untuk mendapatkan hadiah khusus. |
| 10 | Menggunakan Elemental System | User dapat menggunakan elemental system untuk mengalahkan musuh dan memecahkan teka-teki dalam game. |

Berikut adalah class diagram untuk Genshin Impact:

```
+----------------+        +---------------+
| Character      |        | Weapon        |
+----------------+        +---------------+
| name: String   |        | name: String  |
| level: int     |        | type: String  |
| element: String|        | rarity: int   |
| exp: int       |        | attack: int   |
| ...            |        | ...           |
+----------------+        +---------------+
       |                         |
       | has-a                   | has-a
       |                         |
+------+---------+      +--------+--------+
| Player         |      | Enemy           |
+----------------+      +----------------+
| inventory: List |      | name: String    |
| party: List     |      | level: int      |
| ...             |      | health: int     |
+----------------+      | attack: int     |
                         | elemental: int  |
                         | ...            |
                         +----------------+
```

Penjelasan:
- `Character`: Kelas abstrak untuk merepresentasikan karakter dalam game.
- `Player`: Kelas untuk merepresentasikan pemain dalam game yang memiliki `inventory` untuk menyimpan senjata dan peralatan, serta `party` yang berisi daftar karakter yang sedang digunakan.
- `Weapon`: Kelas untuk merepresentasikan senjata dalam game yang memiliki `name`, `type`, `rarity`, `attack`, dan atribut lainnya.
- `Enemy`: Kelas untuk merepresentasikan musuh dalam game yang memiliki `name`, `level`, `health`, `attack`, `elemental`, dan atribut lainnya. Kelas ini juga merupakan turunan dari kelas `Character`.
