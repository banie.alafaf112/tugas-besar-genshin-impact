# No 1

Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat (Lampirkan link source code terkait)

Jawab :

Contoh masalah yang akan kita selesaikan kali ini adalah mencari nilai ATK (Attack) tertinggi dari karakter dalam permainan, dengan mempertimbangkan statistik dan elemen senjata, artefak, dan atribut karakter.

```python 
# import library yang dibutuhkan
import pandas as pd

# load data dari file csv
df_weapon = pd.read_csv('weapon_stats.csv')
df_artifact = pd.read_csv('artifact_stats.csv')
df_character = pd.read_csv('character_stats.csv')

# tentukan elemen senjata dan artefak karakter
weapon_element = 'Pyro'
artifact_element = 'Pyro'

# ambil data karakter dengan elemen dan senjata tertentu
df_filtered_character = df_character.loc[(df_character['element'] == artifact_element) & (df_character['weapon'] == weapon_element)]

# inisialisasi variabel untuk mencari nilai ATK tertinggi
max_atk = 0
max_atk_name = ''

# loop untuk mencari karakter dengan nilai ATK tertinggi
for index, row in df_filtered_character.iterrows():
    char_name = row['name']
    weapon_atk = df_weapon.loc[df_weapon['name'] == row['weapon']]['atk'].iloc[0]
    artifact_atk = df_artifact.loc[df_artifact['name'] == row['artifact']]['atk'].iloc[0]
    base_atk = row['base_atk']
    total_atk = base_atk + weapon_atk + artifact_atk
    
    if total_atk > max_atk:
        max_atk = total_atk
        max_atk_name = char_name

# cetak hasil
print(f"Karakter dengan ATK tertinggi ({max_atk} ATK) adalah {max_atk_name}.")

```
kita menggunakan library Pandas untuk memuat data senjata, artefak, dan karakter dari file CSV. Selanjutnya, kita menentukan elemen senjata dan artefak yang ingin digunakan untuk mencari karakter dengan ATK tertinggi.

Kemudian, kita mengambil data karakter yang memenuhi kriteria elemen dan senjata yang diinginkan dengan menggunakan method loc pada dataframe. Setelah itu, kita menggunakan loop untuk mencari karakter dengan nilai ATK tertinggi.

Dalam setiap iterasi loop, kita mengambil nilai ATK dari senjata, artefak, dan atribut dasar karakter. Total nilai ATK kemudian dihitung dan dibandingkan dengan nilai maksimum yang sudah kita simpan sebelumnya. Jika nilai ATK yang dihitung lebih tinggi dari nilai maksimum, maka kita akan memperbarui nilai maksimum dan nama karakter.

Setelah loop selesai, kita mencetak hasil nilai ATK tertinggi dan nama karakter yang sesuai dengan kriteria yang telah ditentukan.

Dalam algoritma ini, kita menggunakan pendekatan pemrograman berorientasi objek (object-oriented programming) dan juga memanfaatkan operasi dasar pada Pandas seperti filter, join, dan agregasi data untuk mencari nilai ATK tertinggi dari karakter dengan elemen dan senjata tertentu.

# No 2

Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

Jawab :

Algoritma yang digunakan dalam contoh kode di atas adalah sebagai berikut:

1. Import library yang dibutuhkan, yaitu Pandas.
2. Muat data senjata, artefak, dan karakter dari file CSV.
3. Tentukan elemen senjata dan artefak yang ingin digunakan untuk mencari karakter dengan ATK tertinggi.
4. Filter data karakter dengan elemen dan senjata tertentu menggunakan method `loc`.
5. Inisialisasi variabel untuk mencari nilai ATK tertinggi dan nama karakter.
6. Loop untuk setiap karakter yang telah difilter.
7. Ambil nilai ATK dari senjata, artefak, dan atribut dasar karakter.
8. Hitung total nilai ATK.
9. Bandingkan total nilai ATK dengan nilai maksimum yang telah tersimpan sebelumnya.
10. Jika nilai ATK yang dihitung lebih tinggi dari nilai maksimum, maka perbarui nilai maksimum dan nama karakter.
11. Setelah loop selesai, cetak hasil nilai ATK tertinggi dan nama karakter.

Dalam algoritma ini, langkah-langkah yang diambil melibatkan penggunaan pemrograman berorientasi objek dan pemilihan operasi dasar Pandas untuk memfilter data dan melakukan operasi agregasi. Hal ini memungkinkan kita untuk menyelesaikan masalah yang kompleks dengan cara yang relatif mudah dan efisien.

# No 3

Mampu menjelaskan konsep dasar OOP

Jawab : 

Konsep OOP (Object-Oriented Programming) adalah cara pemrograman yang memungkinkan pengembang untuk membangun program dengan mengorganisir kode ke dalam objek yang saling berinteraksi. Berikut ini adalah empat konsep OOP yang penting:

1. Encapsulation (Pembungkusan)
Encapsulation adalah konsep OOP yang menempatkan data dan metode yang berkaitan dalam satu tempat (kelas) dan menyembunyikan akses langsung ke data dari luar kelas. Hal ini memungkinkan data untuk diatur dengan cara yang aman dan memastikan bahwa tidak ada yang dapat mengubah data tanpa mengikuti aturan yang ditetapkan oleh kelas.

2. Inheritance (Pewarisan)
Inheritance adalah konsep OOP di mana kelas baru dapat dibuat dengan mewarisi properti dan metode dari kelas yang sudah ada. Konsep ini memungkinkan pengembang untuk membuat hierarki kelas yang terstruktur dengan kelas induk sebagai dasar untuk kelas-kelas anaknya.

3. Polymorphism (Polimorfisme)
Polymorphism adalah konsep OOP yang memungkinkan pengembang untuk menggunakan kelas yang berbeda sebagai pengganti satu sama lain. Hal ini memungkinkan pengembang untuk menulis kode yang lebih fleksibel dan mudah dimodifikasi.

4. Abstraction (Abstraksi)
Abstraction adalah konsep OOP di mana pengembang dapat memfokuskan pada fitur penting suatu objek dan mengabaikan detail yang tidak penting. Konsep ini memungkinkan pengembang untuk membuat kelas yang lebih abstrak dan mudah digunakan oleh orang lain.

# No 4

Mampu mendemonstrasikan penggunaan Encapsulation secara tepat (Lampirkan link source code terkait)

Jawab : 

Private ditandai dengan (__) pada atribut atau methodnya. Contoh dalam program saya sebagai berikut :

```python
class Player():
    def _init_(self, username: str, password: str, ):
        self.__username = username
        self.__password = PWD_CONTEXT.hash(password)
        self.__hero_owned = list()
        self.__hero_owned_by_dict = dict()
        self.__player_location_x: int =0
        self.__player_location_y: int =0
        self.__explored_maps= {}
        
    def get_username(self):
        return self.__username
    
    def get_password(self):
        return self.__password
    
    def get_hero_owned(self):
        return self.__hero_owned
    
    def get_hero_owned_by_dict(self):
        return self.__hero_owned_by_dict
    
    def get_player_location_x(self):
        return self.__player_location_x
    
    def get_player_location_y(self):
        return self.__player_location_y
    def get_explored_maps(self):
        return self.__explored_maps
    def set_explored_maps(self, name: str, map):
        self.__explored_maps.update({name: map})

```
# No 5

Mampu mendemonstrasikan penggunaan Abstraction secara tepat  (Lampirkan link source code terkait)

Jawab :

Abstraction adalah konsep OOP di mana pengembang dapat memfokuskan pada fitur penting suatu objek dan mengabaikan detail yang tidak penting. Konsep ini memungkinkan pengembang untuk membuat kelas yang lebih abstrak dan mudah digunakan oleh orang lain.

Dalam program ini, abstraction dilakukan dengan membuat method yang abstract dan hanya memberikan deklarasi tanpa implementasi di superclass.

```python 
from abc import ABC, abstractmethod

class Character(ABC):
    def __init__(self, name: str, level: int, hp: int, element: str):
        self.#name = name
        self.#level = level
        self.#hp = hp
        self.#element = element

    @abstractmethod
    def attack(self):
        pass

    @abstractmethod
    def special(self):
        pass

    @abstractmethod
    def defense(self):
        pass

    @abstractmethod
    def use_item(self):
        pass

    def level_up(self):
        self.#level += 1

    def set_element(self, element: str):
        self.#element = element

class PlayableChar(Character):
    def __init__(self, name: str, level: int, hp: int, element: str, stamina: int):
        super().__init__(name, level, hp, element)
        self.#stamina = stamina

    @abstractmethod
    def sprint(self):
        pass

    @abstractmethod
    def glide(self):
        pass

    @abstractmethod
    def charge(self):
        pass

class NonPlayable(Character):
    def __init__(self, name: str, level: int, hp: int, element: str, dialogue: str):
        super().__init__(name, level, hp, element)
        self.#dialogue = dialogue

    @abstractmethod
    def talk(self):
        pass

```

# No 6

Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

Jawab : 

```python 
class Game():
        def start_game(self):
        clear_screen(0)
        iterate_text('continue_text', self.player, 0, 0)
        chosen_maps = choose_maps()
        self.set_map(chosen_maps)
        current_map = self.get_map()
        print(f"check if exits: {self.player.get_explored_maps().get(current_map)}")
        if self.player.get_explored_maps().get(current_map) is None:
            self.player.set_explored_maps(current_map.get_name(), current_map)
        print(f"check if exits second: {self.player.get_explored_maps()[current_map.get_name()]}")
        time.sleep(2)

class Tutorial(Game):
    def _init_(self, player: Player, game_level: int = 1):
        self.is_loopable = True
        self.steps_of_tutorial = {
            1: self.tutorial_intro,
            2: self.get_hero_information,
            3: self.end_of_tutorial
        }
        super()._init_(player, game_level)
        # self.
    
    def start_game(self):
        while self.is_loopable:
            # disable = keyboardDisable()
            # disable.stop()
            get_tutorial = self.intro_and_get_tutorial()
            if get_tutorial.lower() == 'y':
                for i in range(1, len(self.steps_of_tutorial)+1):
                            
                    if i == 1 or i == len(self.steps_of_tutorial):
                        self.steps_of_tutorial[i]()
                        
                    else:
                        proceed_or_not = self.proceed_to_next_tutorial_func()
                        if proceed_or_not == 'y': #move to next tutorial if player chose to carry on the tutorial
                            self.steps_of_tutorial[i]()
                        
                        elif proceed_or_not == 'n': #will end the tutorial if the player chose not to continue the tutorial
                            write_per_character("Alright, then.",0.05, 1.5)
                            write_per_character(" Take care when you're in the wild world of this fantasy for I no longer be with you.",0.05,1.5)
                            break
                        
            else:
                write_per_character("You chose not to take the tutorial...",0.05,1)
                write_per_character(" Enjoy the game!!!",0.05,1.5)
                print()
                # time.sleep(2)
            self.is_loopable = False

```

# No 7

Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

Jawab : 

Proses bisnis dalam gameplay Genshin Impact melibatkan interaksi antara karakter yang dimainkan oleh pemain dan lingkungan serta karakter lain dalam game. Untuk mengimplementasikan proses bisnis ini dalam OOP, kita dapat menggunakan beberapa kelas yang merepresentasikan objek-objek dalam game, seperti karakter, senjata, elemen, dan lain sebagainya.

Berikut adalah beberapa use case dalam gameplay Genshin Impact dan bagaimana kita dapat mengimplementasikannya dalam OOP:

1. Menambahkan karakter
    - Dalam OOP, kita dapat membuat kelas PlayableChar dan NonPlayableChar yang merepresentasikan karakter dalam game.
    - Method add_character pada kelas Game akan membuat instance dari kelas karakter dan menambahkannya ke list character dalam game.

2. Memilih karakter untuk dimainkan
    - Dalam OOP, kita dapat membuat method select_character pada kelas Game yang akan menampilkan daftar karakter yang tersedia dan meminta input dari pengguna untuk memilih karakter yang ingin dimainkan.

3. Menyerang musuh
    - Dalam OOP, kita dapat membuat method attack pada kelas PlayableChar yang akan mengurangi HP musuh berdasarkan attack power dari karakter yang melakukan serangan.
    - Method attack juga dapat digunakan pada NonPlayableChar untuk memungkinkan karakter tersebut melakukan serangan pada pemain.

4. Menggunakan skill khusus karakter
    - Dalam OOP, kita dapat membuat method special pada kelas PlayableChar yang akan mengurangi HP musuh secara signifikan berdasarkan skill khusus karakter yang dipilih.

5. Meningkatkan level karakter
    - Dalam OOP, kita dapat membuat method level_up pada kelas PlayableChar yang akan meningkatkan level karakter dan memberikan peningkatan pada atribut karakter, seperti HP dan attack power.

6. Menggunakan elemen karakter
    - Dalam OOP, kita dapat membuat kelas Element yang merepresentasikan elemen dalam game, seperti api, air, dan lain sebagainya.
    - Method set_element pada kelas PlayableChar akan mengubah elemen karakter menjadi elemen yang dipilih oleh pengguna.

7. Berbicara dengan karakter non-playable
    - Dalam OOP, kita dapat membuat kelas NonPlayableChar yang merepresentasikan karakter non-playable dalam game.
    - Method talk pada kelas NonPlayableChar akan menampilkan dialog dari karakter non-playable yang dipilih oleh pengguna.

Implementasi OOP pada proses bisnis dalam gameplay Genshin Impact memungkinkan pengembang untuk membuat program yang lebih terstruktur dan mudah diatur, dengan menggunakan konsep-konsep seperti inheritance, polymorphism, dan abstraction. Selain itu, OOP juga memungkinkan untuk membuat program yang lebih mudah dikembangkan dan dimodifikasi di masa depan.

# No 8

Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

Jawab :

### Use Case Prioritas

| No | Nama Use Case | Prioritas |
| -- | ------------ | --------- |
| 1  | Memulai Game | Tinggi    |
| 2  | Mekanik Menyerang | Tinggi   |
| 3  | Mekanik Game Over | Tinggi |
| 4  | Mekanik Membuat Hero Berjalan | Tinggi |
| 5  | Mekanik HP Berkurang | Sedang |


### Use Case Scenario

1. Memulai Game
   - User membuka aplikasi Genshin Impact
   - User menekan tombol "Start Game"
   - Sistem memuat data awal dan tampilan menu utama
   
2. Memilih Karakter
   - User memilih menu "Characters"
   - User memilih karakter yang ingin dimainkan
   - Sistem menampilkan detil karakter
   
3. Menyelesaikan Quest Utama
   - User memilih quest utama dari menu "Quests"
   - Sistem menunjukkan arah dan jarak ke tujuan quest
   - User menjelajahi dunia game dan menyelesaikan quest utama
   
4. Memilih Peta Tempat Bermain
   - User memilih menu "Maps"
   - User memilih peta tempat bermain yang ingin dijelajahi
   - Sistem menampilkan peta dan lokasi di dalamnya
   
5. Menyelesaikan World Quest
   - User memilih world quest dari menu "Quests"
   - Sistem menunjukkan arah dan jarak ke tujuan quest
   - User menjelajahi dunia game dan menyelesaikan world quest
   
6. Berinteraksi dengan NPC
   - User menemukan NPC di dalam dunia game
   - User menekan tombol "Interact" untuk berbicara dengan NPC
   - Sistem menampilkan dialog dan pilihan interaksi
   
7. Bermain dengan Teman
   - User membuka menu "Multiplayer"
   - User bergabung dengan sesi game teman atau membuat sesi sendiri
   - Sistem menghubungkan pemain dan menampilkan tampilan game bersama
   
8. Membeli Item dengan Uang Asli
   - User memilih menu "Shop"
   - User memilih item yang ingin dibeli
   - User melakukan transaksi pembayaran menggunakan uang asli
   - Sistem mengirimkan item ke akun user
   
### Class Diagram

```
+------------------+       +-----------------+       +---------------+
| Player           |       | Character       |       | NPC           |
+------------------+       +-----------------+       +---------------+
| - name: string   |       | - name: string  |       | - name: string|
| - level: int     |       | - level: int    |       | - quest: Quest|
| - exp: int       |       | - exp: int      |       | - dialog: []  |
| - stamina: int   |       | - weapon: Weapon|       +---------------+
| - quests: Quest[]|       +-----------------+
| - inventory: []  |
+------------------+

+-------------------+       +-----------------+       +---------------+
|+-------------------+       +-----------------+       +---------------+
| Weapon            |       | Quest           |       | Shop          |
+-------------------+       +-----------------+       +---------------+
| - name: string    |       | - name: string  |       | - items: []   |
| - type: string    |       | - reward: []    |       | - price: int  |
| - rarity: string  |       | - status: string|       +---------------+
| - damage: int     |       +-----------------+
+-------------------+

```

Penjelasan class diagram:

- `Player`: class untuk merepresentasikan pemain dalam game Genshin Impact. Memiliki atribut seperti `name`, `level`, `exp`, `stamina`, `quests`, dan `inventory`.
- `Character`: class untuk merepresentasikan karakter dalam game Genshin Impact. Memiliki atribut seperti `name`, `level`, `exp`, dan `weapon`.
- `NPC`: class untuk merepresentasikan karakter non-player dalam game Genshin Impact. Memiliki atribut seperti `name`, `quest`, dan `dialog`.
- `Weapon`: class untuk merepresentasikan senjata dalam game Genshin Impact. Memiliki atribut seperti `name`, `type`, `rarity`, dan `damage`.
- `Quest`: class untuk merepresentasikan quest dalam game Genshin Impact. Memiliki atribut seperti `name`, `reward`, dan `status`.
- `Shop`: class untuk merepresentasikan toko dalam game Genshin Impact. Memiliki atribut seperti `items` dan `price`.



### Berikut adalah list use case prioritas dari gameplay Genshin Impact:

| No. | Use Case Prioritas | Deskripsi |
| --- | --- | --- |
| 1 | Menjelajahi Teyvat | User dapat menjelajahi dunia Teyvat dengan kebebasan untuk menentukan rute yang ingin diambil. |
| 2 | Bertarung dengan Musuh | User dapat bertarung dengan musuh menggunakan sistem pertarungan yang bervariasi dan mendebarkan. |
| 3 | Memilih Karakter | User dapat memilih karakter yang sesuai dengan gaya bermain mereka dan membangun tim karakter yang kuat. |
| 4 | Meningkatkan Level Karakter | User dapat meningkatkan level dan skill karakter mereka untuk meningkatkan kemampuan bertarung dan mengakses konten yang lebih sulit. |
| 5 | Mendapatkan Senjata dan Peralatan Baru | User dapat mendapatkan senjata dan peralatan baru untuk memperkuat karakter mereka. |
| 6 | Memainkan Story Quests | User dapat memainkan story quest untuk mengikuti cerita utama dan mengalami pengalaman storytelling yang menarik. |
| 7 | Memainkan World Quests | User dapat memainkan world quest untuk menambah pengalaman dan mendapatkan hadiah. |
| 8 | Berinteraksi dengan NPC | User dapat berinteraksi dengan karakter non-player dalam game untuk memperoleh informasi atau quest baru. |
| 9 | Memainkan Event | User dapat memainkan event untuk mendapatkan hadiah khusus. |
| 10 | Menggunakan Elemental System | User dapat menggunakan elemental system untuk mengalahkan musuh dan memecahkan teka-teki dalam game. |

### Berikut adalah class diagram untuk Genshin Impact:

```
+----------------+        +---------------+
| Character      |        | Weapon        |
+----------------+        +---------------+
| name: String   |        | name: String  |
| level: int     |        | type: String  |
| element: String|        | rarity: int   |
| exp: int       |        | attack: int   |
| ...            |        | ...           |
+----------------+        +---------------+
       |                         |
       | has-a                   | has-a
       |                         |
+------+---------+      +--------+--------+
| Player         |      | Enemy           |
+----------------+      +----------------+
| inventory: List |      | name: String    |
| party: List     |      | level: int      |
| ...             |      | health: int     |
+----------------+      | attack: int     |
                         | elemental: int  |
                         | ...            |
                         +----------------+
```

Penjelasan:
- `Character`: Kelas abstrak untuk merepresentasikan karakter dalam game.
- `Player`: Kelas untuk merepresentasikan pemain dalam game yang memiliki `inventory` untuk menyimpan senjata dan peralatan, serta `party` yang berisi daftar karakter yang sedang digunakan.
- `Weapon`: Kelas untuk merepresentasikan senjata dalam game yang memiliki `name`, `type`, `rarity`, `attack`, dan atribut lainnya.
- `Enemy`: Kelas untuk merepresentasikan musuh dalam game yang memiliki `name`, `level`, `health`, `attack`, `elemental`, dan atribut lainnya. Kelas ini juga merupakan turunan dari kelas `Character`.

# No 9

Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

Jawab : 



# No 10

Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

Jawab : 

