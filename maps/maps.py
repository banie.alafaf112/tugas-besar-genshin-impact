from player.player import Player

maps = list()
maps_2 = dict()

class Maps():
    def __init__(self, name: str, width: int, height: int):
        self.__name = name
        self.__width = width
        self.__height = height
        self.__tiles = {
            "10x10": {
                "monsters": "basic"
            }
        }
        self.__player: Player =None
        
    def get_width(self):
        return self.__width
    
    def get_name(self):
        return self.__name
    
    def get_height(self):
        return self.__height
    
    def get_tiles(self):
        return self.__tiles
    
    def get_player(self):
        return self.__player
    
    def set_player(self, value: Player|None):
        self.__player = value
        