import time
from maps.maps import maps, maps_2 , Maps
from utilities import write_per_character, clear_screen
map_list = [
    {
        "name": "Tevyat",
        # "width": 15,
        # "height": 30,
        # "tiles" : {
        #     ""
        # }
        "width": 1500,
        "height": 3000,
    },
    {
        "name": "Enkanomiya",
        # "width": 12,
        # "height": 29,
        "width": 1245,
        "height": 2900,
    },
]

def print_maps():
    print()
    for i, map in enumerate(maps):
        write_per_character(f"{i+1}. {map['name']}", 0.05, 0.3)
        print()
        # time.sleep(1)
def choose_maps():
    
    print_maps()
    write_per_character("Choose map: ", 0.05, 0)
    choose_maps_var = input("")
    choose_maps_var = int(choose_maps_var)
    while not choose_maps_var >=1 or not choose_maps_var <= len(maps):
        if not choose_maps_var >=1 or not choose_maps_var <= len(maps):
            clear_screen(0)
            write_per_character(f"There are only {len(maps)} maps. Please select from 1 to {len(maps)}", 0.05, 0)
            print_maps()
            write_per_character("\nChoose map: ", 0.05, 0)
            choose_maps_var = input("")
            choose_maps_var = int(choose_maps_var)
        else:
            print("Break while")
            clear_screen()
            time.sleep(3)
            return maps[choose_maps_var-1]
        
    if choose_maps_var >=1 or choose_maps_var <= len(maps):
        print("Hello world from choose maps var bigger or equals one and choose map var less than or equals total maps")
        print(f"selected maps: {maps[choose_maps_var-1]['name']}")
        time.sleep(2)
        return maps[choose_maps_var-1]
    # else:
    #     print("Break while")
def generate_maps():
    for map in map_list:
        new_map = Maps(map['name'], map['width'], map['height'])
        maps.append({'name': map["name"], 'map': new_map})
        
        maps_2.update({map['name']: {'map': new_map}})