import sys

from passlib.context import CryptContext
from character.hero.hero import Hero
from pynput.keyboard import *

# import maps.maps
# sys.path.append(r'D:\PROGRAMMING\python\bani\Genshin-Impact-OOP-master\New Genshin Impact')



PWD_CONTEXT = CryptContext(schemes=["bcrypt"], deprecated="auto")

player_list = list()
player_list_2 = dict()

class Player():
    def __init__(self, username: str, password: str, ):
        self.__username = username
        self.__password = PWD_CONTEXT.hash(password)
        self.__hero_owned = list()
        self.__player_location_x: int =0
        self.__player_location_y: int =0
        
    def get_username(self):
        return self.__username
    
    def get_password(self):
        return self.__password
    
    def get_hero_owned(self):
        return self.__hero_owned
    
    def get_player_location_x(self):
        return self.__player_location_x
    
    def get_player_location_y(self):
        return self.__player_location_y
    
    def set_player_location_x(self, value: int):
        self.__player_location_x = self.__player_location_x + value
    
    def set_player_location_y(self, value: int):
        self.__player_location_y = self.__player_location_y + value
    
    def add_hero_collection(self, value: Hero):
        self.__hero_owned.append(value)
        
    def player_input(self, key, current_map):
        if key == Key.right:
            self.__player_location_x = self.__player_location_x + 1
            if self.get_player_location_x() > current_map.get_width():
                self.__player_location_x = current_map.get_width()
            # self.set_player_location_x(1)
            # self.x_pos = self.x_pos + 1
            
        if key == Key.left:
            self.__player_location_x = self.__player_location_x - 1
            # self.x_pos = self.x_pos - 1
            if self.get_player_location_x() <0:
                self.__player_location_x = 0
            
                
        if key == Key.up:
            self.__player_location_y = self.__player_location_y + 1
            if self.get_player_location_y() > current_map.get_height():
                self.__player_location_y = current_map.get_height()
            
        if key == Key.down:
            self.__player_location_y = self.__player_location_y - 1
            # self.y_pos = self.y_pos - 1
            if self.get_player_location_y() < 0:
                self.__player_location_y = 0
        
        # clear_screen(0)
        print(f"x_pos⏳: {self.get_player_location_x()}, y_pos: {self.get_player_location_y()}",end='\r')